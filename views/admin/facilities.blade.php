<x-admin-component>
        <!-- main sidebar -->
        <div class="main-content">
            

            <div class="row g-10">
                
                <div class="col-xl-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" style="color: #3b82f6;">Add Category</button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="fireSweetAlert()" style="color: #FF0800;">Delete Category</button>

                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterLabel">Add Category</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputCategoryname">Category Name</label>
                                                    <input type="text" class="form-control" id="exampleInputCategoryname" placeholder="Category Name">
                                                </div>
                                                
                                                <!-- <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputFile">File input</label>
                                                    <input class="form-control" type="file" id="exampleInputFile">
                                                </div> -->
                                    
                                    
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" onclick="fireSweetAlertCategorySaved()">Save Category</button>                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                

                <div class="panel mb-10 mt-10">
                    <nav>
                        <!-- <a href="#" class="sidebar-link active">Sports</a>
                        <a href="#">Conference</a>
                        <a href="#">MPH</a>
                        <a href="#">Bus</a>
                        <a href="#">Guest House</a> -->
                        
                        <a class="nav-link active" data-target="#sportsTable">Sports</a>
                        <a class="nav-link" data-target="#conferenceTable">Conference</a>
                        <a class="nav-link" data-target="#mphTable">MPH</a>
                        <a class="nav-link" data-target="#busTable">Bus</a>
                        <a class="nav-link" data-target="#guestTable">Guest House</a>

                        <div class="panel-body">
                            <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalCenterFacility" style="color: #3b82f6;">Add Facility</button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="fireSweetAlertFacility()" style="color: #FF0800;">Delete Facility</button>
                        </div>
                        <div class="modal fade" id="exampleModalCenterFacility" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterLabel">Add Facility</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputFacilityname">Name</label>
                                                    <input type="text" class="form-control" id="exampleInputFacilityname" placeholder="Name">
                                                </div>
                                                
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputDetails">Facility</label>
                                                    <input type="text" class="form-control" id="exampleInputDetails" placeholder="Facility">
                                                </div>

                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="exampleInputFile">File input</label>
                                                    <input class="form-control" type="file" id="exampleInputFile">
                                                </div>
                                    
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" onclick="fireSweetAlertFacilitySaved()">Save Facility</button>                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </nav>
                    
                    <div class="col-xl-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-striped table-hover" id="sportsTable">
                                <thead>
                                  <tr>
                                    <th>
                                      <span class="custom-checkbox">
                                                        <input type="checkbox" id="selectAll">
                                                        <label for="selectAll"></label>
                                                    </span>
                                    </th>
                                    <th>Name</th>
                                    <th>Facility</th>
                                    <th>Actions</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <span class="custom-checkbox">
                                              <input type="checkbox" id="checkbox1" name="options[]" value="1">
                                              <label for="checkbox1"></label>
                                                    </span>
                                    </td>
                                    <td>Football ground</td>
                                    <td>football</td>
                                    
                                    <td>
                                    <!-- <a href="#editEmployeeModal" class="edit" data-toggle="modal" data-bs-target="#exampleModalCenterFacility">
                                            <i class="bi bi-pencil-square" data-toggle="tooltip" title="Edit"></i></a> -->
                                    <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalCenterEditFacility" style="color: #3b82f6;">Edit</button>
                                    <div class="modal fade" id="exampleModalCenterEditFacility" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalCenterLabel">Edit Facility</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <div class="form-group mb-20">
                                                            <label class="form-label" for="exampleInputFacilityname">Name</label>
                                                            <input type="text" class="form-control" id="exampleInputFacilityname" placeholder="Name">
                                                        </div>
                                                        
                                                        <div class="form-group mb-20">
                                                            <label class="form-label" for="exampleInputDetails">Facility</label>
                                                            <input type="text" class="form-control" id="exampleInputDetails" placeholder="Facility">
                                                        </div>
                                            
                                            
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" onclick="fireSweetAlertEditSaved()">Save</button>                                        
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <a href="#deleteEmployeeModal" class="delete" data-toggle="modal">
                                            <i class="bi bi-trash" data-toggle="tooltip" title="Delete"></i></a> -->
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <span class="custom-checkbox">
                                                        <input type="checkbox" id="checkbox2" 
                                                          name="options[]" value="1">
                                                        <label for="checkbox2"></label>
                                                    </span>
                                    </td>
                                    <td>Basketball court</td>
                                    <td>Basketball</td>
                                    
                                    <td>
                                        <button type="button" class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalCenterEditFacility" style="color: #3b82f6;">Edit</button>
                                    </td>
                                  </tr>
                                  
                                   
                                    
                                  
                                </tbody>
                                </table>

                                

                             

                                

                               
                        </div>
                    </div>
                </div>
                   
                </div>

            </div>
            
            
        </div>
    </div>

    <script>
  
        // Initally only show sports table
        $('.table').addClass('d-none'); 
        $('#sportsTable').removeClass('d-none');

        $('.nav-link').click(function() {

        // Toggle active class
        $('.nav-link').removeClass('active');
        $(this).addClass('active');

        // Show/hide tables
        var target = $(this).data('target');
        $('.table').addClass('d-none');  
        $(target).removeClass('d-none');

        });

    </script>

    <script>

    function fireSweetAlert() {
        Swal.fire({
            title: 'Are you sure?',
            
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#5cb85c',
            cancelButtonColor: '#d9534f',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                'Deleted!',
                'Category has been deleted.',
                'success'
                )
            }
            })
    }
    </script>
    <script>
        function fireSweetAlertFacility() {
            Swal.fire({
                title: 'Are you sure?',
                
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#5cb85c',
                cancelButtonColor: '#d9534f',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Deleted!',
                    'Facility has been deleted.',
                    'success'
                    )
                }
                })
        }
    </script>
    <script>
        function fireSweetAlertCategorySaved() {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Category has been saved',
                showConfirmButton: false,
                timer: 1500
                })
        }
    </script>

    <script>
        function fireSweetAlertFacilitySaved() {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Facility has been saved',
                showConfirmButton: false,
                timer: 1500
                })
        }
    </script>

    <script>
        function fireSweetAlertEditSaved() {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Update has been saved',
                showConfirmButton: false,
                timer: 1500
                })
        }
    </script>

</x-admin-component>