<x-app-layout>
    
   

    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!-- ======= Portfolio Section ======= -->
                    <section id="portfolio" class="portfolio">
                        <div class="container">
                            <div class="row" data-aos="fade-up" data-aos-delay="200">
                                <div class="col-lg-12 d-flex justify-content-center">
                                    <ul id="portfolio-flters">
                                        <li data-filter="*" class="filter-active">All</li>
                                        <li data-filter=".filter-app">Conference</li>
                                        <li data-filter=".filter-card">Sport</li>
                                        <li data-filter=".filter-web">MPH</li>
                                        <li data-filter=".filter-bus">Bus</li>
                                        <li data-filter=".filter-gh">Guest House</li>
                                    </ul>
                                </div>
                            </div>
                    
                            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="400">
                    
                                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                                <div class="portfolio-wrap">
                                    <img src="{{ asset('user_dashboard/img/c.png') }}" class="img-fluid" alt="">
                                    <div class="portfolio-info">
                                    <h4>Meeting Room 1</h4>
                                    {{-- <p>Description</p> --}}
                                    <div class="portfolio-links">
                                        <a href="{{ url('detail') }}" title="More Details">View Detail</a>
                                        
                                    </div>
                                    </div>
                                </div>
                                </div>
                    
                                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                                <div class="portfolio-wrap">
                                    <img src="{{ asset('user_dashboard/img/f.png') }}" class="img-fluid" alt="">
                                    <div class="portfolio-info">
                                    <h4>BasketBall</h4>
                                  
                                    <div class="portfolio-links">
                                        <a href="{{ url('detail') }}" title="More Details">View Detail</a>                                      
                                    </div>
                                    </div>
                                </div>
                                </div>
                    
                                <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                                <div class="portfolio-wrap">
                                    <img src="{{ asset('user_dashboard/img/m.png') }}" class="img-fluid" alt="">
                                    <div class="portfolio-info">
                                    <h4>MPH</h4>
                                   
                                    <div class="portfolio-links">
                                        <a href="{{ url('detail') }}" title="More Details">View Detail</a>                                     
                                    </div>
                                    </div>
                                </div>
                                </div>
                    
                    
                                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                                <div class="portfolio-wrap">
                                    <img src="{{ asset('user_dashboard/img/f.png') }}" class="img-fluid" alt="">
                                    <div class="portfolio-info">
                                    <h4>Football</h4>
                                  
                                    <div class="portfolio-links">
                                        <a href="{{ url('detail') }}" title="More Details">View Detail</a>                                        
                                    </div>
                                    </div>
                                </div>
                                </div>
                    
                                <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                                <div class="portfolio-wrap">
                                    <img src="{{ asset('user_dashboard/img/f.png') }}" class="img-fluid" alt="">
                                    <div class="portfolio-info">
                                    <h4>VolleyBall</h4>
                                  
                                    <div class="portfolio-links">
                                        <a href="{{ url('detail') }}" title="More Details">View Detail</a>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="col-lg-4 col-md-6 portfolio-item filter-gh">
                                    <div class="portfolio-wrap">
                                        <img src="{{ asset('user_dashboard/img/g.png') }}" class="img-fluid" alt="">
                                        <div class="portfolio-info">
                                        <h4>Guest House</h4>
                                      
                                        <div class="portfolio-links">
                                            <a href="{{ url('detail') }}" title="More Details">View Detail</a>    
                                        </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 portfolio-item filter-bus">
                                    <div class="portfolio-wrap">
                                        <img src="{{ asset('user_dashboard/img/bus.png') }}" class="img-fluid" alt="">
                                        <div class="portfolio-info">
                                        <h4>Bus 1</h4>
                                       
                                        <div class="portfolio-links">
                                            <a href="{{ url('detail') }}" title="More Details">View Detail</a>    
                                        </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-6 portfolio-item filter-bus">
                                    <div class="portfolio-wrap">
                                        <img src="{{ asset('user_dashboard/img/bus.png') }}" class="img-fluid" alt="">
                                        <div class="portfolio-info">
                                        <h4>Bus 2</h4>
                                      
                                        <div class="portfolio-links">
                                            <a href="{{ url('detail') }}" title="More Details">View Detail</a>    
                                        </div>
                                        </div>
                                    </div>
                                </div>
                    
                            </div>
                        </div>
                    </section><!-- End Portfolio Section -->
                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
