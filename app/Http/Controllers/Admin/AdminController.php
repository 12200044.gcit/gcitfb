<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function facilities()
    {
        return view('admin.facilities');
    }
    public function user()
    {
        
        $users = User::all();
        return view('admin.user', compact('users'));
    }
    public function booking()
    {
        return view('admin.booking');
    }
    public function setting()
    {
        return view('admin.setting');
    }
    public function userform(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            // 'password' => ['required', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            // 'password' => Hash::make($request->password),
        ]);
        $users = User::all(); 
        echo '<script>fireSweetAlertAddUser();</script>';

        return view('admin.user', compact('users'))->with('success', 'Registration successful.');
    }
    
}
