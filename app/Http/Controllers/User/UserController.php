<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function detail()
    {
        return view('detail');
    }
    public function mybooking()
    {
        return view('mybooking');
    }
    public function booknow()
    {
        return view('booknow');
    }
}
