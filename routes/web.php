<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\User\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

///////////////////////////////////////////////////////////////////////////////////////////////
Route::middleware(['auth','role:admin'])->group(function () {
    Route::get('admin/dashboard', [AdminController::class, 'dashboard']);
    Route::get('admin/facilities', [AdminController::class, 'facilities']);
    Route::get('admin/booking', [AdminController::class, 'booking']);
    Route::get('admin/setting', [AdminController::class, 'setting']);
    Route::get('admin/user', [AdminController::class, 'user']);
    Route::post('admin/userform', [AdminController::class, 'userform'])->name('admin.userform');
});




Route::middleware(['auth','role:user'])->group(function () {
    Route::get('detail', [UserController::class, 'detail']);
    Route::get('mybooking', [UserController::class, 'mybooking']);
    Route::get('booknow', [UserController::class, 'booknow']);

});




