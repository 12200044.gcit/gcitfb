<x-app-layout>
   

    <div class="py-3">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <!-- ======= Portfolio Section ======= -->
                    <section id="portfolio" class="portfolio">
                        <div class="container">
                            
                    
                            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="400">
                    
                                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                                <div class="portfolio-wrap">
                                    <img src="{{ asset('user_dashboard/img/c.png') }}" class="img-fluid" alt="">
                                    <div class="portfolio-info">
                                        <h4>Meeting Room 1</h4>
                                        
                                    </div>
                                </div>
                                </div>
                    
                                
                    
                            </div>
                            <div class="portfolio-info " data-aos="fade-up" data-aos-delay="400">
                                <ul>
                                  <li><strong>Category</strong>: Conference</li>
                                  <li><strong>Facility</strong>: Chair, Desk, Projector</li>
                                  <li><strong>Date</strong>: 12/10/2023</li>
                                  <li><strong>Time</strong>: 9am to 10am</li>
                                  <li class="btn btn-primary" style="background-color: #3498db"><strong>Status</strong>: Booked</li>
                                </ul>
                            </div>
                        </div>
                    </section><!-- End Portfolio Section -->
                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
