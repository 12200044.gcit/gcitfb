

<x-admin-component>
    

<!-- main sidebar -->
<div class="main-content">
    

    <div class="row g-10">
        <div class="col-xl-6 col-lg-6">
            <div class="panel">
                <div class="panel-body">
                    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{ asset('admin_assets/images/all facility 1.png') }}" class="d-block w-100" alt="slide image">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>All Facilities</h5>
                                    <h3>25</h3>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="{{ asset('admin_assets/images/all category.png') }}" class="d-block w-100" alt="slide image">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>All Category</h5>
                                    <h3>5</h3>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="{{ asset('admin_assets/images/all booking.png')}}" class="d-block w-100" alt="slide image">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Recent Bookings</h5>
                                    <h3>7</h3>
                                </div>
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6">
            <div class="panel mb-10">
                <div class="panel-heading">
                    <span>Recent Bookings</span>
                   <a href="{{ url ('admin/booking')}}"> <button type="button" class="btn btn-outline-primary" >View more</button> </a>                       </div>
                <div class="panel-body">
                    <div class="list-group">
                        <!-- <a href="#" class="list-group-item list-group-item-action active" aria-current="true">
                            <div class="d-flex w-100 justify-content-between">
                                <p class="mb-1">12200051.gcit@rub.edu.bt</p>
                                <small>3 days ago</small>
                            </div>
                            <p class="mb-1">Football Ground</p>
                        </a> -->
                        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                            <div class="d-flex w-100 justify-content-between">
                                <p class="mb-1">12200051.gcit@rub.edu.bt</p>
                                <!-- <small>3 days ago</small> -->
                            </div>
                            <p class="mb-1">Football Ground</p>
                        </a>
                        <a href="#" class="list-group-item list-group-item-action">
                            <div class="d-flex w-100 justify-content-between">
                                <p class="mb-1">12200051.gcit@rub.edu.bt</p>
                                <!-- <small class="text-muted">3 days ago</small> -->
                            </div>
                            <p class="mb-1">Football Ground</p>
                        </a>
                        <a href="#" class="list-group-item list-group-item-action">
                            <div class="d-flex w-100 justify-content-between">
                                <p class="mb-1">2200051.gcit@rub.edu.bt</p>
                                <!-- <small class="text-muted">3 days ago</small> -->
                            </div>
                            <p class="mb-1">Football Ground</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel col-xl-12 col-lg-12 mb-10 mt-10">
            <div class="panel-heading">
                <span>Category</span>
                <!-- <div>
                    <button type="button" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" style="background-color: green !important; border-color: green !important">Add</button>
                    <button type="button" class="btn btn-danger btn-sm" >Delete</button>
                    
                </div> -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterLabel">Add Category</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group mb-20">
                                        <label class="form-label" for="exampleInputEmail1">Category name</label>
                                        <input type="text" class="form-control" id="exampleInputTexts">
                                    </div>
                                    
                                    <div class="form-group mb-20">
                                        <label class="form-label" for="exampleInputFile">Upload image</label>
                                        <input class="form-control" type="file" id="exampleInputFile">
                                    </div>
                                    
                                </form>                                    
                            </div>
                            <div class="modal-footer">
                                <!-- <a href="#">
                                    <button type="button" class="btn btn-primary">Save</button>
                                </a> -->
                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal" aria-label="Close">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row g-2 mb-3">
            <div class="col-xxl-3 col-lg-4 col-sm-6">
                <div class="card border" style="border-radius: 10px;">
                    <!-- <div class="card-header">Header</div> -->
                    <div class="panel-body">
                        <div class="card text-white">
                            <img src="{{ asset('admin_assets/images/Sports 1.png')}}" class="card-img" alt="card image">
                            <div class="card-img-overlay">
                                <div class="text-center">
                                    <h5 class="card-title">Sports</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-3 col-lg-4 col-sm-6">
                <div class="card border" style="border-radius: 10px;">
                    <!-- <div class="card-header">Header</div> -->
                    <div class="panel-body">
                        <div class="card text-white">
                            <img src="{{ asset('admin_assets/images/MPH 1.png')}}" class="card-img" alt="card image">
                            <div class="card-img-overlay">
                                <div class="text-center">
                                    <h5 class="card-title">MPH</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-3 col-lg-4 col-sm-6">
                <div class="card border" style="border-radius: 10px;">
                    <!-- <div class="card-header">Header</div> -->
                    <div class="panel-body">
                        <div class="card text-white">
                            <img src="{{ asset('admin_assets/images/Guest house 1.png')}}" class="card-img" alt="card image">
                            <div class="card-img-overlay">
                                <div class="text-center">
                                    <h5 class="card-title">Guest House</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-3 col-lg-4 col-sm-6">
                <div class="card border" style="border-radius: 10px;">
                    <!-- <div class="card-header">Header</div> -->
                    <div class="panel-body">
                        <div class="card text-white">
                            <img src="{{ asset('admin_assets/images/ConferenceRoom 1.png')}}" class="card-img" alt="card image">
                            <div class="card-img-overlay">
                                <div class="text-center">
                                    <h5 class="card-title">Conference Room</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-3 col-lg-4 col-sm-6">
                <div class="card border" style="border-radius: 10px;">
                    <!-- <div class="card-header">Header</div> -->
                    <div class="panel-body">
                        <div class="card text-white">
                            <img src="{{ asset('admin_assets/images/Transportation 1.png')}}" class="card-img" alt="card image">
                            <div class="card-img-overlay">
                                <div class="text-center">
                                    <h5 class="card-title">Transportation</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

    </div>
    
    
</div>
</x-admin-component>
