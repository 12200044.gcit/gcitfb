<x-admin-component>
        <div class="main-content">
            <div class="row g-10">
            <div class="col-xl-12 col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" style="background-color: #3498db">Add User</button>
                            <button type="button" class="btn btn-danger" onclick="fireSweetAlertDeleteUser()" style="background-color: red">Delete User</button>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" >
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterLabel">Add User</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{ route('admin.userform') }}">
                                                @csrf
                                                <!-- Name -->
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="name">Name</label>
                                                    <input id="name" class="form-control" type="text" name="name" :value="{{ old('name') }}" required autofocus autocomplete="name" />   
                                                </div>
                                                <!-- Email Address -->
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="email">Email</label>
                                                    <input id="email" class="form-control" type="email" name="email" value="{{ old('email') }}" required autocomplete="username" />    
                                                </div>
                                                <!-- User Type -->
                                                <div class="form-group mb-20">
                                                    <label class="form-label" for="usertype">User Type</label>    
                                                    <input id="usertype" class="form-control" type="text" name="usertype" value="{{ old('usertype') }}" required autocomplete="usertype" /> 
                                                </div>
                                               <div class="form-group mb-20">
                                                <label class="form-label" for="role">User Role:</label>&nbsp;
                                                <input id="role_user" class="form-control" type="radio" name="role" value="user" {{ old('role') == 'user' ? 'checked' : '' }} required autocomplete="role">&nbsp;User&nbsp;
                                                <input id="role_admin" class="form-control" type="radio" name="role" value="admin" {{ old('role') == 'admin' ? 'checked' : '' }} required autocomplete="role">&nbsp;Admin
                                               </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-success" style="background-color: #3498db; color:white" onsubmit="fireSweetAlertAddUser()">
                                                        Add
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="panel mb-10 mt-10">
                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" style="background-color: #3498db">Add User</button>
                    <button type="button" class="btn btn-danger" onclick="fireSweetAlertDeleteUser()" style="background-color: red">Delete User</button>
                    <div class="col-xl-12 col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>
                                        <span class="custom-checkbox">
                                            <input type="checkbox" id="selectAll">
                                            <label for="selectAll"></label>
                                        </span>
                                        </th>
                                        <th>Sl_No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <th>Role</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $serialNumber = 1;
                                        @endphp

                                        @foreach($users as $user)
                                    <tr>
                                        <td>
                                        <span class="custom-checkbox">
                                            <input type="checkbox" name="user_ids[]" value="{{ $user->id }}">                                             
                                        </span>
                                        </td>
                                        <td>{{ $serialNumber }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->usertype }}</td>
                                        <td>{{ $user->role }}</td>    
                                    </tr>
                                        @php
                                            $serialNumber++;
                                        @endphp
                                    @endforeach           
                                    </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>       
        </div>
    <script>  
        function fireSweetAlertDeleteUser() {
            Swal.fire({
                title: 'Are you sure you want to Delete User?',
                
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#5cb85c',
                cancelButtonColor: '#d9534f',
                confirmButtonText: 'Yes, Delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Deleted'
                    )
                }
                })
        }
    </script>
    <script>
        function fireSweetAlertAddUser() {
            Swal.fire(
                {
                position: 'top',
                icon: 'success',
                title: 'User has been saved',
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>
</x-admin-component>