<x-admin-component>
   
        <div class="main-content">
            <div class="row g-10">
               
                    
                        
                            
                           
                                <div class="py-12">
                                    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
                                        <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                                            <div class="max-w-xl">
                                                @include('profile.partials.update-profile-information-form')
                                            </div>
                                        </div>
                            
                                        <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                                            <div class="max-w-xl">
                                                @include('profile.partials.update-password-form')
                                            </div>
                                        </div>

                                        <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                                            <div class="max-w-xl">
                                                <button type="button" class="btn btn btn-secondary" data-bs-toggle="modal" data-bs-target="#exampleModalFull" style="background-color: #3498db; color:white">
                                                    Edit
                                                </button>&nbsp;<span>Terms and conditions</span>
                                            </div>
                                        </div>
                            
                            
                                        {{-- <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                                            <div class="max-w-xl">
                                                @include('profile.partials.delete-user-form')
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            
                 
                

            </div>
            
                
                <div class="modal fade" id="exampleModalFull" tabindex="-1" aria-labelledby="exampleModalFullLabel" aria-hidden="true">
                    <div class="modal-dialog modal-fullscreen">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalFullLabel">Edit Terms and conditions</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="post">
                                    <textarea id="mytextarea">Hello, World!</textarea>
                                </form>                            
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" onclick="fireSweetAlertEditTC()">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        
 

    <script>
        function fireSweetAlertUpdatePassword() {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Update Password has been saved',
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>
    <script>
        function fireSweetAlertEditTC() {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Changes has been saved',
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>
    
</x-admin-component>